<?php

/**
 * Autoloader a classok betöltésére
 * @param string $class_name
 */
function __autoload($class_name) {
    include 'class.' . $class_name . '.inc';
}

//adatok az urlből
$p = filter_input(INPUT_GET, 'p') ? filter_input(INPUT_GET, 'p') : 'crud';
//módok leválasztása
$mode = filter_input(INPUT_GET, 'action') ? filter_input(INPUT_GET, 'action') : 'list'; //vagy van mód, vagy list
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ? filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) : NULL;
//post feldolgozás
//var_dump($_POST);
$errors = [];
if (filter_input(INPUT_POST, 'submit')) {
//    var_dump($_POST);die();
    //ha van id akkor töltsük be a címet
    if ($tid) {
        try {
            $address = Address::load($tid);
            $address->address_type_id = filter_input(INPUT_POST, 'address_type_id', FILTER_VALIDATE_INT); //hogy a form szerinti állapotban legyen
            try {
//                var_dump($address,$address->toArray());
                $address = Address::getInstance($address->address_type_id, $address->toArray());
            } catch (ExceptionAddress $e) {
                echo $e;
            }
        } catch (ExceptionAddress $e) {
            echo $e;
        }
    } elseif (filter_input(INPUT_POST, 'address_type_id', FILTER_VALIDATE_INT)) {//ha nincs id akkor typeid alapján készítsünk üreset
        $classname = 'Address' . Address::$valid_address_types[filter_input(INPUT_POST, 'address_type_id')];
        $address = new $classname;
    }
    //egy input elem validálás
    $address->street_address_1 = filter_input(INPUT_POST, 'street_address_1');
    if (!$address->street_address_1) {
        $errors['street_address_1'] = ' alert alert-danger ';
    }
    //
    $address->street_address_2 = filter_input(INPUT_POST, 'street_address_2');
    $address->city_name = filter_input(INPUT_POST, 'city_name');
    $address->subdivision_name = filter_input(INPUT_POST, 'subdivision_name');
    if (filter_input(INPUT_POST, 'postal_code')) {
        $address->postal_code = filter_input(INPUT_POST, 'postal_code');
    } else {
        unset($address->postal_code);
    }
    $address->country_name = filter_input(INPUT_POST, 'country_name');
//    echo '<pre>' . var_export($address, true) . '</pre>';
    if (empty($errors)) {//nincs hiba, mentsünk és mehetünk a listára
        $address->save();
        header('location:?p=' . $p);
        exit();
    }
//    echo '<pre>' . var_export($address, true) . '</pre>';
}
?>
<div class="row">
    <div class="col-md-12">
        <?php
        $title = $btn = false;

        switch ($mode) {
            case 'del':
                if ($tid) {//ha törölni kell törlünk
                    try {
                        $address_delete = Address::load($tid);
                        $address_delete->delete();
                    } catch (ExceptionAddress $e) {
                        echo $e;
                    }
                }
                header('location:?p=' . $p);
                exit();
                break;
            case 'mod':
                $title = '<h4>Cím módosítás: - ' . $tid . ' -</h4>';
                $btn = 'Módosítok';
                //lekérjük az adatokat a form kitöltéshez
                if (!isset($address)) {
                    try {
                        $address = Address::load($tid);
                    } catch (ExceptionAddress $e) {
                        echo $e;
                    }
                }
            //var_dump($address);
            //break;
            case 'new':
                if (!isset($address)) {
                    $address = new AddressResidence;
                }
                $title = !$title ? '<h4>Új cím felvitele</h4>' : $title;
                $btn = !$btn ? 'Új cím felvitele' : $btn;
                //typelist
                $address_type_list = '';
                foreach (Address::$valid_address_types as $typeid => $typename) {
                    $address_type_list .= '<option value="' . $typeid . '" ' . ($address->address_type_id == $typeid ? 'selected' : '') . '>' . $typename . '</option>';
                }

                $form = '<form method="post">' . $title . '<div class="form-group row">
                            <label for="address_type_id" class="col-2 col-form-label">Címtipus</label>
                            <div class="col-10">
                                <select name="address_type_id" class="form-control"  id="address_type_id">
                                ' . $address_type_list . '</select>
                            </div>
                         </div>';
                $form .= '<div class="form-group row">
                            <label for="street_address_1" class="col-2 col-form-label">Címsor 1.</label>
                            <div class="col-10">
                                <input name="street_address_1" class="form-control ' .
                        (array_key_exists('street_address_1', $errors) ? $errors['street_address_1'] : '')
                        . '" type="text" value="' . $address->street_address_1 . '" id="street_address_1" ></div>
                         </div>';
                $form .= '<div class="form-group row">
                            <label for="street_address_2" class="col-2 col-form-label">Címsor 2.</label>
                            <div class="col-10">
                                <input name="street_address_2" class="form-control" type="text" value="' . $address->street_address_2 . '" id="street_address_2">
                            </div>
                         </div>';
                $form .= '<div class="form-group row"> 
                            <div class="col-10">
                               <label for="postal_code" class="col-3 col-form-label">Irányítószám<input name="postal_code" class="form-control" type="text" value="' . $address->postal_code . '" id="postal_code" readonly></label>
                               <label for="city_name" class="col-3 col-form-label">Városnév<input name="city_name" class="form-control" type="text" value="' . $address->city_name . '" id="city_name"></label>  
                               <label for="subdivision_name" class="col-3 col-form-label">Város rész<input name="subdivision_name" class="form-control" type="text" value="' . $address->subdivision_name . '" id="subdivision_name" readonly></label>
                            </div> <div id="searchresults" class="col-xs-6">valami</div>
                         </div>';

                $form .= '<div class="form-group row">
                            <label for="country_name" class="col-2 col-form-label">Ország név</label>
                            <div class="col-10">
                                <input name="country_name" class="form-control" type="text" value="' . $address->country_name . '" id="country_name">
                            </div>
                         </div>';
                $form .= '<button name="submit" type="submit" class="btn btn-primary" value="submit">' . $btn . '</button>'
                        . '</form>';
                echo $form;
                break;
            default : $addresses = Address::getAll();
                ?>

                <h4>Eddig rögzített címek</h4>
                <div class="table-responsive">

                    <a class="col-xs-12 btn btn-success" href="?p=<?php echo $p; ?>&amp;action=new">új cím</a>
                    <table id="mytable" class="table table-bordered table-striped">
                        <thead>
                        <th><input type="checkbox" id="checkall" /></th>
                        <th>id</th>
                        <th>Név</th>
                        <th>Cím</th>
                        <th>Típus</th>
                        <th>Szerkeszt</th>
                        <th>Töröl</th>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($addresses as $k => $address) {
                                echo '<tr>
                        <td><input type="checkbox" class="checkthis" /></td>
                        <td>' . $address['address_id'] . '</td>
                        <td>Teszt név</td>
                        <td>' . $address['postal_code']
                                . ' | ' . $address['city_name']
                                . ', ' . $address['subdivision_name'] . ' ' . $address['street_address_1'] . ' ' . $address['street_address_2'] . '</td>
                        <td>' . Address::$valid_address_types[$address['address_type_id']] . '</td>
                        <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="?p=crud&amp;action=mod&amp;tid=' . $address['address_id'] . '" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                        <td><p data-placement="top" data-toggle="tooltip" title="Delete"><a href="?p=crud&amp;action=del&amp;tid=' . $address['address_id'] . '" class="btn btn-danger btn-xs" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></a></p></td>
                    </tr>';
                            }
                            ?>

                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <!--            <ul class="pagination pull-right">
                                    <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                </ul>-->

                </div>
                <?php
                break; //default
        }
        ?>
    </div>
</div>
<!-- js a kereséshez -->
<script type="text/javascript">

    jQuery(document).ready(function ($) {
        /*bill leütésvizsgálata*/
        $('#city_name').on('keyup', function () {
            //console.log('A beírt szöveg hossza:' + $(this).val().length);
            if ($(this).val().length > 2) {
                makeAjaxRequest();

                return false;
            } else {
                $('#searchresults').css('display', 'none');
                $('#searchresults').html('');

            }
        });

        function makeAjaxRequest() {

            $.ajax({
                url: 'ajaxsearch.php',
                type: 'get',
                data: {city_name: $('input#city_name').val()},
                success: function (response) {
                    $('#searchresults').css('display', 'block');
                    $('#searchresults').html(response);
                    $("#searchresults li").click(function () {
                        var li = $(this);
                        //console.log( $(this).text() );

                        //betöltjük a kivánt input value értékébe az adott elem textjét
                        $("#postal_code").val(li.data("postal_code"));
                        $("#city_name").val(li.data("city"));
                        $("#subdivision_name").val(li.data("subdivision"));

                        $("#searchresults").slideUp();
                    });
                }
            });
        }
    });


</script>