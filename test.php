<?php

//osztály betöltése
include('class.AddressBase.inc');
include('class.Database.inc');
//objektum létrehozása
$address = new AddressBase;
echo '<h1>Új üres objektum</h1>';
echo '<pre>' . var_export($address, true) . '</pre>';

//objektum feltöltése adatokkal
$address->street_address_1 = 'Teszt utca 234.';
$address->city_name = "Budapest";
$address->country_name = "Magyarország";
$address->postal_code = "1234";
$address->address_type_id=1;
echo '<h1>Adatok megadása az objektumnak</h1>';
echo '<pre>' . var_export($address, true) . '</pre>';

//objektum eljárás hívása
echo '<h1>Cím kiírása objektum eljárással (display())</h1>';
echo '<div class="row"><div class=" col-sm-4">'; //most ilyen dobozba kérjük
echo $address->display();
echo '</div></div>';

//védett tulajdonság tesztelése
//echo '<h1>Védett tulajdonság tesztelése</h1>';
//$address->address_id= 12345 ;
//egy adat kiírása az objektumból
echo '<h1>Egy adat kiírása az objektumból</h1>';
echo $address->country_name;
//nem létező objektum tulajdonság megadása
echo '<h1>Nem létező objektum tulajdonság megadása</h1>';
$address->iranyitoszam = "teszt";
echo '<pre>' . var_export($address, true) . '</pre>'; //nem kerül bele már üres __set esetén sem
//védett tulajdonság kiírása
echo '<h1>Védett tulajdonság kiírása</h1>';
echo 'Cím azonosító:' . $address->address_id;

echo $address->valami;

//objektum feltöltése tömb segítségével
$data = [
    'street_address_1' => 'Újabb utca 2.',
    'city_name' => 'Sárospatak',
    'country_name' => 'Magyarország',
    'postal_code' => 1234,
];
$address_2 = new AddressBase($data);
echo '<h1>Objektum feltöltése tömb segítségével</h1>';
echo '<pre>' . var_export($address_2, true) . '</pre>';
//osztály állandó tesztelése - létezik objektum nélkül is
echo '<h1>osztály állandó tesztelése</h1>';
echo '<ul>'
 . '<li>Állandó cím azonosítója:' . AddressBase::ADDRESS_TYPE_RESIDENCE . '</li>'
 . '<li>Számlázási cím azonosítója:' . AddressBase::ADDRESS_TYPE_BUSINESS . '</li>'
 . '<li>Szállítási v. ideiglenes cím azonosítója:' . AddressBase::ADDRESS_TYPE_TEMPORARY . '</li>'
 . '</ul>';

echo AddressBase::$valid_address_types[AddressBase::ADDRESS_TYPE_RESIDENCE];
//címtípus érvényesség tesztelése - statikus eljárás segítségével
echo '<h1>Címtípus érvényesség tesztelése</h1>';
for($i=0;$i<=5;$i++){
    echo '<br>'.$i.':'.( AddressBase::isValidAddressTypeId($i) ? '' : 'nem ').'érvényes';
}
//objektum kiírása magic method segítségével
echo '<h1>objektum kiírása magic method segítségével</h1>';
echo $address;
//gyakorlás
//valós vidéki címadatok feltöltése objektumba
$data = [
    'street_address_1' => 'Újabb utca 2.',
    'city_name' => 'sár',
    'country_name' => 'Magyarország',
//    'postal_code' => 1234,
];
$address_search = new AddressBase($data);

echo $address_search;

//típuskényszerítés avagy typecasting
$valami= (object) 'hello world' ;
var_dump($valami);

//statikus kereső eljárás tesztelése
echo '<h1>Statikus kereső eljárás tesztelése</h1>';
$addresspart=AddressBase::search_addressparts_from_db();
echo '<pre>'.var_export($addresspart,true).'</pre>';