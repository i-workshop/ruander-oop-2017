<?php
//vizsgáljuk meg hogy paraméterből mit kapunk, milyen belső filet szeretnénk includeolni
$includeFile = (filter_input(INPUT_GET, 'p') ? filter_input(INPUT_GET, 'p') : 'test') . '.php';
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ruander OOP - PHP tanfolyam</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="css/custom.css" rel="stylesheet">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">OOP tanfolyam</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Alap</a></li>
                        <li><a href="?p=form">Űrlap</a></li>
                        <li><a href="?p=extensions">Bővítések</a></li>
                        <li><a href="?p=crud">CRUD tábla</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
        <div class="container">
            <div class="row">
                <header class="col-xs-12">
                    <h1>Ruander OOP - PHP tanfolyam</h1> 
                </header>
            </div>
            <div class="row">
                <article class="col-xs-12">
                    <?php
                    if (file_exists($includeFile)) {
                        include($includeFile);
                    } else {
                        echo 'nem tudom a kívánt filet betölteni!'; //dismissable alert
                    }
                    ?>
                </article>
            </div>
            <footer class="row">
                <div class="col-md-6 col-xs-12">
                    Minden tartalom védett &copy; Ruander <?php echo date('Y'); ?>
                </div>
                <div class="col-md-6 col-xs-12">
                    Készítette: Horváth György
                </div>
            </footer>
        </div>

    </body>
</html>
