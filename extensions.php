<?php
/**
 * Autoloader a classok betöltésére
 * @param string $class_name
 */
function __autoload($class_name){
    include 'class.'.$class_name.'.inc';
}
/* az address abstract ezért nem példányosítható
$address=new Address;
echo '<pre>'.var_export($address,true).'</pre>';
*/
$data=[
    'street_address_1' => 'Újabb utca 2.',
    'city_name' => 'Halásztelek',
    'country_name' => 'Magyarország',
];
$address_residence = new AddressResidence($data);
$id=$address_residence->save();
echo '<h2>Adatbázis beillesztés sikeres, a beillesztett cím azonosítója: '.$id.'</h2>';

echo '<pre>'.var_export($address_residence,true).'</pre>';
echo $address_residence;
$data=[
    'street_address_1' => 'Teszt tér 2.',
    'city_name' => 'Jakabszállás',
    'country_name' => 'Magyarország',
];
$address_business = new AddressBusiness($data);
$id=$address_business->save();
echo '<h2>Adatbázis beillesztés sikeres, a beillesztett cím azonosítója: '.$id.'</h2>';
echo '<pre>'.var_export($address_business,true).'</pre>';
echo $address_business;
$data=[
    'street_address_1' => 'Fő utca 57.',
    'city_name' => 'Szentes',
    'country_name' => 'Magyarország',
];
$address_temporary = new AddressTemporary($data);

echo '<pre>'.var_export($address_temporary,true).'</pre>';
echo $address_temporary;
//$id=$address_temporary->save();
echo '<h2>Adatbázis beillesztés sikeres, a beillesztett cím azonosítója: '.$id.'</h2>';
$address_db = Address::load(2);

echo '<h1>Cím betöltése adatbázisból cím azonosító (address_id) alapján</h1>';
echo $address_db;
$address_db->street_address_1 = "Megváltozott tér 10.";
echo $address_db;
$address_db->save();


echo '<h1>Cím törlése adatbázisból cím azonosító (address_id) alapján</h1>';

try{
    $address_db = Address::load(200);
    echo $address_db;
    $address_db->delete();
}catch(ExceptionAddress $e){
    echo $e;
//    echo '<pre>'.var_export($e,true).'</pre>';
}

$data=[
    'street_address_1' => 'Újabb utca 2.',
    'city_name' => 'Halásztelek',
    'country_name' => 'Magyarország',
];
$address_db_2 = Address::getInstance(0, $data);