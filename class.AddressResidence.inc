<?php

/*
 * Address osztály bővítése
 */

class AddressResidence extends Address {

    //public $country_name = 'USA'; //property redeclare
    //display eljárás felülírása - method override
    public function display() {
        $output = '<div class="panel panel-primary alert-info">
                    <div class="panel-heading">
                      <h3 class="panel-title">Állandó lakcím</h3>
                    </div>';
        $output .= parent::display();
        $output .= '</div>';

        return $output;
    }

    protected function _init() {
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_RESIDENCE);
    }

}
