<?php
/**
 * Database class. Csak egyetlen kapcsolatot engedünk (singleton)
 */
class Database {
    //kapcsolat
    private $_connection;
    //példány
    private static $_instance;
    /**
     * db példány visszaadása, csak akkor készül új ha még nincs
     * @return db instance
     */
    public static function getInstance() {
        if(!self::$_instance){
            self::$_instance = new self;
        }
        return self::$_instance;
    }
    /**
     * Magic __clone üresen a klónozásvédelemre
     */
    final public function __clone(){}
    
    /**
     * Magic __construct
     */
    public function __construct() {
        $this->_connection = new mysqli('localhost', 'root', '', 'hgy_oop');
        
        //hibekezelés
        if(mysqli_connect_error()){
            trigger_error('Hiba az adatbázis kapcsolat létrehozása során:'.mysqli_connect_error(), E_USER_ERROR);
        }   
    }
    /**
     * ckapcsolodás visszadása
     * @return connection
     */
    public function getConnection(){
        return $this->_connection;
    }
    
    
}