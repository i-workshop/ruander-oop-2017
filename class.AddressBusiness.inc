<?php

/*
 * Address osztály bővítése
 */

class AddressBusiness extends Address {

    //display eljárás felülírása - method override
    public function display() {
        $output = '<div class="panel panel-warning">
                    <div class="panel-heading">
                      <h3 class="panel-title">Számlázási cím</h3>
                    </div>';
        $output .= parent::display();
        $output .= '</div>';

        return $output;
    }
    
    protected function _init() {
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_BUSINESS);
    }

}
