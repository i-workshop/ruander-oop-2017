<?php

/*
 * saját hibaüzenet osztály az Addresshez
 */

class ExceptionAddress extends Exception {

    public function __toString() {
        return __CLASS__ . ": [$this->code] - $this->message ".PHP_EOL;
    }

}
