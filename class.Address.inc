<?php

/**
 * Címkezelő osztály
 */
abstract class Address implements Model {

    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;
    //hibakódok
    const ADDRESS_ERROR_NOT_FOUND = 1000;
    const ADDRESS_ERROR_SUBCLASS_DOESNT_EXISTS = 1001;

    //érvényes címtípusok statikus tömbje
    //mindkét kulcs megadás fajta működik, de a self szebb és praktikusabb
    public static $valid_address_types = [
        Address::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary',
    ];
    //címsor 1
    public $street_address_1;
    //címsor 2
    public $street_address_2;
    //város
    public $city_name;
    //város rész
    public $subdivision_name;
    //irányítószám
    protected $_postal_code;
    //ország
    public $country_name;
    //felh azonosító
    public $user_id; //későbbi felh. csatolás előkészítése
    //cím azonosítója
    protected $_address_id;
    //címtípus azonosítója
    protected $_address_type_id;
    //létrehozás és módosítás dátuma
    protected $_time_created;
    protected $_time_updated;

    /**
     * konstruktor
     * akkor fut amikor egy new Classname fut
     */
    public function __construct($data = []) {
        //var_dump('fut a konstruktor:', $data);
        $this->_init(); //type id beállítása a bővítésekből
        $this->_time_created = time();
        //ellenőrizzük hogy a kapott adattípus feldolgozható-e
        if (!is_array($data)) {
            trigger_error('Nem tudunk felépíteni objektumot a(z) ' . get_class($name) . ' osztály segítségével, mert hibás a kapott adattípus [' . gettype($data) . ']');
        }
        //ha nem üres a tömb, megpróbáljuk felépíteni az objektumot a kapott adatokból
        if (count($data) > 0) {
            foreach ($data as $name => $value) {
                //kivételek a védett tulajdonságoknak-pl ha db ből szedjük az adatokat
                if (in_array($name, [
                            'time_created',
                            'time_updated',
                            'address_id',
                            'address_type_id'
                        ])) {
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }
        }
    }

    /**
     * Magic __get
     * akkor fut amikor egy nem elérhető tulajdonságot próbálunk meg az objektumból elérni
     * @param string $name
     */
    public function __get($name) {
        //nézzük meg nincs e védett tulajdonság ugyanebből
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }

        $protected_property_name = '_' . $name; //ha van védett akkor ez lesz a neve
        //ha ez létezik, állítsuk be
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }
        //Nem lehet a tulajdonságot elérni, trigger error
        trigger_error('Nem létező vagy védett tulajdonságot próbálunk elérni __get által (' . $name . ')');
        return NULL;
    }

    /**
     * Magic __set
     * akkor fut amikor egy nem létező tulajdonságot próbálunk megadni az objektumnak
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value) {
        //címtípus megadthatóság biztosítása
        if ($name == 'address_type_id') {
            $this->_setAddressTypeId($value);
            return;
        }
        //Irányítószám, ha van azt engedjük
        if ($name == 'postal_code') {
            $this->$name = $value;
            return; //ezt ne feletsd le a kivételek beállítása után különben triggerelsz az eljárás végén
        }

        //hibakezelés nem létező tulajdonságra
        trigger_error('Nem létező tulajdonságot probálunk beállítani __set által (' . $name . ')');
    }

    /**
     * Magic _toString az objektumok közvetlen kiírhatóságához
     * @return string
     */
    public function __toString() {
        return $this->display();
    }

    //követeljük meg a bővítésektől hogy tartalmazzanak egy  _init eléjárást amiben beállítják az address_type_id -t
    abstract protected function _init();

    /**
     * Eljárás irányítószám kereséshez adott város és városrész ismeretében
     * @todo a több visszatérést le kell tudni kezelni - nem egyértelmű meghatározás üzenettel
     * @return string
     */
    protected function _postal_code_search() {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8"); //kódlap illesztése
        if ($this->city_name == '')
            return 'üres városnév';
        $qry = "SELECT *"
                . " FROM telepulesek";
        $city_name = $mysqli->real_escape_string($this->city_name); //város név
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name); //város rész név
        $qry .= " WHERE telepules_nev LIKE '%$city_name%'"
                . " AND telepules_resz = '$subdivision_name' "
                . " ORDER BY telepules_nev ";
        // echo $qry;
        $result = $mysqli->query($qry) or die('Gebasz');
        if ($result->num_rows == 1) {//ha csak 1 találat van, minden ok, vissza is adjuk
            $row = $result->fetch_assoc();
            return $row['irsz'];
        } else { //bejárjuk az kapott eredményeket
            while ($row = $result->fetch_assoc()) {
                //var_dump('<pre>', $row, '</pre>');
            }
        }
        return 'több van';
    }

    /**
     * Cím kereséshez készült statikus kereső eljárás
     * @param string $city
     * @return mixed (null vagy array)
     */
    public static function search_addressparts_from_db($city = '') {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        $qry = "SELECT *"
                . " FROM telepulesek";
        $city_name = $mysqli->real_escape_string($city); //város név
        
        $qry .= " WHERE telepules_nev LIKE '%$city_name%'"
                //  . " AND telepules_resz = '$subdivision_name' "
                . " ORDER BY telepules_nev ";
        $result = $mysqli->query($qry) or die('Gebasz');
        $data = [];//itt lesznek az eredmények
        while ($row = $result->fetch_assoc()) {
            $data[] = [
                $row['irsz'],
                $row['telepules_nev'], 
                $row['telepules_resz']
                ];
        }
        return $data;
    }

    /**
     * Címtípus érvényességének vizsgálata
     * @param int $address_type_id
     * @return boolean
     */
    public static function isValidAddressTypeId($address_type_id) {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Címtipus azonosító beállítása ha érvényes
     * @param int $address_type_id
     * @return void
     */
    protected function _setAddressTypeId($address_type_id) {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
            return;
        }
        //
    }

    /**
     * Cím kiírása
     * uses bootstrap classes
     * @return string
     */
    public function display() {

        $output = '<div class="panel-body">'; //panel head
        //cím adatok
        $output .= '<div>' . $this->street_address_1;
        $output .= '<br>' . $this->street_address_2 . '</div>';
        $output .= '<div>' . $this->postal_code . ', ' . $this->city_name . '</div>';
        //ha van városrész név akkor azt is kiírjuk
        if ($this->subdivision_name) {
            $output .= '<div>' . $this->subdivision_name . '</div>';
        }
        $output .= '<div>' . $this->country_name . '</div>';
        $output .= '</div>'; //panel closing

        return $output;
    }

    /**
     * Minden tárolt cím lekérése
     * @param int $slice
     * @param int $from
     * @return associative array
     */
    public static function getAll($slice = 50, $from = 0) {
        $db = Database::getInstance();
        //csatlakozás
        $mysqli = $db->getConnection();
        $mysqli->set_charset('utf8'); //kódlap illesztés
        $qry = "SELECT * FROM addresses LIMIT $from,$slice";
        $result = $mysqli->query($qry) or die($mysqli->error);
        $data = $result->fetch_all(MYSQLI_ASSOC);
        //var_dump($data);die();
        return $data;
    }

    /**
     * Load
     */
    final public static function load($address_id) {

        $db = Database::getInstance();
        //csatlakozás
        $mysqli = $db->getConnection();
        $mysqli->set_charset('utf8'); //kódlap illesztés
        $qry = "SELECT * ";
        $qry .= " FROM addresses ";
        $qry .= " WHERE address_id = " . (int) $address_id;
        $qry .= " LIMIT 1 ";

        $result = $mysqli->query($qry) or die($mysqli->error);

        if ($row = $result->fetch_assoc()) {
            return self::getInstance($row['address_type_id'], $row);
        }
        throw new ExceptionAddress('Cím nem található!', self::ADDRESS_ERROR_NOT_FOUND);
    }

    /**
     * save
     */
    final public function save() {
        $db = Database::getInstance();
        //csatlakozás
        $mysqli = $db->getConnection();
        $mysqli->set_charset('utf8'); //kódlap illesztés       
        //adatok escapelése
        $city_name = $mysqli->real_escape_string($this->city_name); //város név
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name); //város rész név
        $street_address_1 = $mysqli->real_escape_string($this->street_address_1);
        $street_address_2 = $mysqli->real_escape_string($this->street_address_2);
        $postal_code = $mysqli->real_escape_string($this->postal_code);
        $country_name = $mysqli->real_escape_string($this->country_name);
        //todo:új felvitel és meglévő módosításának szétválasztása
        if ($this->_address_id == NULL) {
            $time_created = date('Y-m-d H:i:s', $this->time_created);
            $user_id = rand(1, 10); //todo:később valós user id kell!!!!!!!!!!!!
            //kérés összeállítása
            $qry = "INSERT INTO `addresses` ("
                    . " `street_address_1`,"
                    . " `street_address_2`, "
                    . "`city_name`, "
                    . "`subdivision_name`, "
                    . "`postal_code`, "
                    . "`country_name`, "
                    . "`address_type_id`, "
                    . "`user_id`, "
                    . "`time_created`, "
                    . "`time_updated`) "
                    . "VALUES ("
                    . " '$street_address_1', "
                    . "'$street_address_2', "
                    . "'$city_name', "
                    . "'$subdivision_name', "
                    . "'$postal_code', "
                    . "'$country_name', "
                    . "'$this->address_type_id', "
                    . "$user_id, "
                    . "'$time_created', "
                    . "NULL);";
            //echo $qry;
            $mysqli->query($qry) or die('Gebasz on save');
            $id = $mysqli->insert_id;
        } else {
            $id = $this->_address_id;
            $this->_time_updated = date('Y-m-d H:i:s');
            $qry = "UPDATE `addresses`"
                    . " SET `street_address_1`='$street_address_1',"
                    . " `street_address_2`='$street_address_2', "
                    . "`city_name`='$city_name', "
                    . "`subdivision_name`='$subdivision_name', "
                    . "`postal_code`='$postal_code', "
                    . "`country_name`='$country_name', "
                    . "`address_type_id`='$this->address_type_id', "
                    . "`time_updated`='$this->_time_updated'"
                    . " WHERE address_id = $id "
                    . "LIMIT 1";
            //echo $qry;
            $mysqli->query($qry) or die('Gebasz on update');
        }
        return $id;
    }

    /**
     * törlés
     * @return mixed (int deleted address_id, false)
     */
    public function delete() {
        //törlés;
        $address_id = $this->_address_id;
        if ($address_id > 0) {
            $db = Database::getInstance();
            //csatlakozás
            $mysqli = $db->getConnection();
            $mysqli->set_charset('utf8'); //kódlap illesztés 
            $qry = "DELETE FROM addresses WHERE address_id = " . (int) $address_id . " LIMIT 1";
            $mysqli->query($qry) or die('GEBASZ on delete');
            return $address_id;
        }
        return false;
    }

    /**
     * Példányosító a megfelelő alosztályon keresztül
     * @param int $address_type_id
     * @param array $data
     * @return object instance
     */
    public static function getInstance($address_type_id, $data = []) {
        $class_name = 'Address';
        if (array_key_exists($address_type_id, self::$valid_address_types)) {
            $class_name .= self::$valid_address_types[$address_type_id];
        }
        $test = new ReflectionClass($class_name);
//        var_dump(get_class_methods($test),$test->isAbstract());
//        die();
        // AddressResidence($data) ha az address_type_id = 1
        if (class_exists($class_name) && !$test->isAbstract()) {
            return new $class_name($data);
        }

        throw new ExceptionAddress('Objektum nem hozható létre a kívánt subclassal: ' . $class_name, Address::ADDRESS_ERROR_SUBCLASS_DOESNT_EXISTS);
    }

    /**
     * tömbbé alakítás
     * @return 
     */
    public function toArray() {
        foreach ($this as $k => $v) {
            $data[$k] = $v;
        }
        return $data;
    }

}
