**PHP OOP tanfolyam** 

 1.  alkalom: tudásszint felmérés 
 2.  alkalom: verziokezelés alapok GIT -tel
 3.  alkalom: elméleti alapok, osztály létrehozása, objektummal kapcsolatos műveletek
        -construct,set,get magic methodok
 4.  alkalom: konstruktor befejezése az adott feladatra(tömbből objektum felépítés), konstans és statikus tulajdonság bevezetése a címtípus megadására és ellenőrzésére
        -static,self,:: operátor
        -eljárások elkészítése a címtípus ellenőrzéshez (public static function, protected function)
 5.  alkalom: magic toString alkalmazása, konkrét feladatlista elkészítése, időbecsléssel, .gitignore alkalmazása _dev mappára
        -címadadatbázis megtervezése felépítése
        -adabázis singleton (egyke) osztály létrehozása
 6. alkalom: fejlesztési esettanulmány. (opensource , pl wordpress)
        -irányítószám keresés városnév vagy városnév részlet alapján az elkészített adatbázis egyke (Database singleton) segítségével
	-a felszínre került elvi hibák megoldásainak kidolgozása, a hibák javítása
 7. alkalom: statikus címkeresés előkészítése
        -Bootstrap navigáció és url vezérelt tartalom betöltés elkészítése
        -Address osztály bővítések és Address osztály abstraktá alakítása (_init megkövetelése a bővítésektől és konstruktorban való futtatása az address type id beállítására)
        -Bővítésből tulajdonság újradeklarálás tesztelése
        -Bővítésből Method override tesztelése (display)
        -Model interface elkészítése a load és save eljárások megkövetelésére
        -Inerface alkalmazása az Address osztályon
 8. alkalom: 
	-AddressBase létrehozása hogy az eredeti Address működés megmaradjon, illetve az alap menüpont működőképes lehessen (abstract és interface előtti állapot)
	-Address::load(id) elkészítése és a példányosítás megvalósítása dinamikus alosztály hivással
	-save elkészítése. HF: getAll eljárás interfaceből megkövetelése és megírása az address osztályban
 9. alkalom:
        -crud tábla létrehozása és database object clone védelem
        -Address save eljárás módosítása, hogy updateljen is ha van id.
        -Exception kezelés nem létező cím betöltésére 1.
        -Exception kezelés nem létező cím betöltésére saját exception kiegészítéssel, és hibakóddal. getInstance kiegészítése ha rossz cimptipusazonosítóval szeretnék meghívni, az batract ősből ne akarjon objektumot felépíteni
        -Törlés megoldása és crudban alkalmazása (url vezérléssel)
10. alkalom:
        -(form hackelés elleni védelem),post esetén objektum type beállítása;todo:crud befejezése, a mostani dumpolt állapot kijavítása 
        -Crud működés újratöltéssel
        -laravel 5.4 telepítés, route-controller-view működés alapok
11. alkalom:
        -teljesen ajaxos címkereső mező működésének megtervezése és kivitzelezése (jQuery)
12. alkalom:
        -c9.io bemutatása, pár apróbb fix
        -adatádadási és blade kezelés módszerek bemutatása
        -Carbon bemutatása

13. alkalom:
        -javascriptes működés implementálása a keresési eredmény listaelemeire, hogy kitöltsék a form mezőit.
        -contact email (kapcsolat oldali űrlap) megvalósítása Laravel keretrendszer alatt
14. alkalom:
        -validálás és hibakezelés, blade műveletek
        -Beépített Auth aktiválása a Laravel keretrendszerben
        -
Migráció készítése , tinker használatának bemutatása, mentés adatbázisba controllerből eloquent segítségével
15. alkalom: 
        -Egy komplex feladat megoldása laravel keretrendszerben (cikkfeltöltés és front end megjelenítés)
        -Összefoglalás, téma zárás