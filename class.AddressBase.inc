<?php

/**
 * Címkezelő osztály
 */
class AddressBase {

    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;

    //érvényes címtípusok statikus tömbje
    //mindkét kulcs megadás fajta működik, de a self szebb és praktikusabb
    public static $valid_address_types = [
        self::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary',
    ];
    //címsor 1
    public $street_address_1;
    //címsor 2
    public $street_address_2;
    //város
    public $city_name;
    //város rész
    public $subdivision_name;
    //irányítószám
    protected $_postal_code;
    //ország
    public $country_name;
    //cím azonosítója
    protected $_address_id;
    //címtípus azonosítója
    protected $_address_type_id;
    //létrehozás és módosítás dátuma
    protected $_time_created;
    protected $_time_updated;

    /**
     * konstruktor
     * akkor fut amikor egy new Classname fut
     */
    public function __construct($data = []) {
        //var_dump('fut a konstruktor:', $data);
        $this->_time_created = time();
        //ellenőrizzük hogy a kapott adattípus feldolgozható-e
        if (!is_array($data)) {
            trigger_error('Nem tudunk felépíteni objektumot a(z) ' . get_class($name) . ' osztály segítségével, mert hibás a kapott adattípus [' . gettype($data) . ']');
        }
        //ha nem üres a tömb, megpróbáljuk felépíteni az objektumot a kapott adatokból
        if (count($data) > 0) {
            foreach ($data as $name => $value) {
                //kivételek a védett tulajdonságoknak-pl ha db ből szedjük az adatokat
                if (in_array($name, [
                            'time_created', 'time_updated'
                        ])) {
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }
        }
    }

    /**
     * Magic __get
     * akkor fut amikor egy nem elérhető tulajdonságot próbálunk meg az objektumból elérni
     * @param string $name
     */
    public function __get($name) {
        //nézzük meg nincs e védett tulajdonság ugyanebből
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }

        $protected_property_name = '_' . $name; //ha van védett akkor ez lesz a neve
        //ha ez létezik, állítsuk be
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }
        //Nem lehet a tulajdonságot elérni, trigger error
        trigger_error('Nem létező vagy védett tulajdonságot próbálunk elérni __get által (' . $name . ')');
        return NULL;
    }

    /**
     * Magic __set
     * akkor fut amikor egy nem létező tulajdonságot próbálunk megadni az objektumnak
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value) {
        //címtípus megadthatóság biztosítása
        if ($name == 'address_type_id') {
            $this->_setAddressTypeId($value);
            return;
        }
        //Irányítószám, ha van azt engedjük
        if ($name == 'postal_code') {
            $this->$name = $value;
            return; //ezt ne feletsd le a kivételek beállítása után különben triggerelsz az eljárás végén
        }

        //hibakezelés nem létező tulajdonságra
        trigger_error('Nem létező tulajdonságot probálunk beállítani __set által (' . $name . ')');
    }

    /**
     * Magic _toString az objektumok közvetlen kiírhatóságához
     * @return string
     */
    public function __toString() {
        return $this->display();
    }
    
    /**
     * Eljárás irányítószám kereséshez adott város és városrész ismeretében
     * @todo a több visszatérést le kell tudni kezelni - nem egyértelmű meghatározás üzenettel
     * @return string
     */
    protected function _postal_code_search() {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8"); //kódlap illesztése
        $qry = "SELECT *"
                . " FROM telepulesek";
        $city_name = $mysqli->real_escape_string($this->city_name); //város név
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name); //város rész név
        $qry .= " WHERE telepules_nev LIKE '%$city_name%'"
                . " AND telepules_resz = '$subdivision_name' "
                . " ORDER BY telepules_nev ";
        // echo $qry;
        $result = $mysqli->query($qry) or die('Gebasz');
        if ($result->num_rows == 1) {//ha csak 1 találat van, minden ok, vissza is adjuk
            $row = $result->fetch_assoc();
            return $row['irsz'];
        } else { //bejárjuk az kapott eredményeket
            while ($row = $result->fetch_assoc()) {
                var_dump('<pre>', $row, '</pre>');                
            }
        }
        return 'több van';
    }
    
    /**
     * Cím kereséshez készült statikus kereső eljárás
     * @param string $city
     * @return mixed (null vagy array)
     */
    public static function search_addressparts_from_db($city=''){
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        $data=['postalcode','city','subdivision'];
        return $data;
    }

    /**
     * Címtípus érvényességének vizsgálata
     * @param int $address_type_id
     * @return boolean
     */
    public static function isValidAddressTypeId($address_type_id) {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Címtipus azonosító beállítása ha érvényes
     * @param int $address_type_id
     * @return void
     */
    protected function _setAddressTypeId($address_type_id) {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
            return;
        }
        //
    }

    /**
     * Cím kiírása
     * uses bootstrap classes
     * @return string
     */
    public function display() {

        $output = '<div class="panel-body">'; //panel head
        //cím adatok
        $output .= '<div>' . $this->street_address_1;
        $output .= '<br>' . $this->street_address_2 . '</div>';
        $output .= '<div>' . $this->postal_code . ', ' . $this->city_name . '</div>';
        //ha van városrész név akkor azt is kiírjuk
        if ($this->subdivision_name) {
            $output .= '<div>' . $this->subdivision_name . '</div>';
        }
        $output .= '<div>' . $this->country_name . '</div>';
        $output .= '</div>'; //panel closing

        return $output;
    }

}
