<?php

/**
 * Autoloader a classok betöltésére
 * @param string $class_name
 */
function __autoload($class_name) {
    include 'class.' . $class_name . '.inc';
}

//ezt keressük:
$search_for = filter_input(INPUT_GET, 'city_name');
$results = Address::search_addressparts_from_db($search_for);
if (!empty($results)) {
    echo '<ul>';
    foreach ($results as $cimek) {
        echo '<li data-postal_code="' . $cimek[0] . '" data-city="' . $cimek[1] . '" data-subdivision="' . $cimek[2] . '">' . implode(', ', $cimek) . '</li>';
    }
    echo '<ul>';
} else {
    echo 'Nincsen találat.';
}
