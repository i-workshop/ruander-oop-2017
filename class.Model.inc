<?php
/**
 * interface az interakcióknak
 */
interface Model{
    
    /**
     * 'Cím' (model) betöltése
     * @param int $address_id
     */
   public static function load($address_id);
    
   /**
    * 'Cím' (model) mentése
    */
   public function save();
   
   /**
    * Törlés
    */
   public function delete();
   /**
    * Eljárás az adott osztály minden elemének lekérésére
    */
   public static function getAll($slice,$from);
   /**
    * tömbbé alakítás
    */
   public function toArray();
}